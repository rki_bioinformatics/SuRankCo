# SuRankCo

Please find the SuRankCo repository at [SuRankCo](https://gitlab.com/mkuhring/SuRankCo).

SuRankCo is a machine learning based software to score and rank contigs from de novo assemblies of next generation sequencing data. It trains with alignments of contigs with known reference genomes and predicts scores and ranking for contigs which have no related reference genome yet.

For more details about SuRankCo and its functioning, please see:  
[SuRankCo: Supervised Ranking of Contigs in de novo Assemblies](http://www.biomedcentral.com/1471-2105/16/240/abstract)  
Mathias Kuhring, Piotr Wojtek Dabrowski, Andreas Nitsche and Bernhard Y. Renard, 2015, BMC Bioinformatics
